PgBouncer role
=========

Installs PgBouncer on Debian/Ubuntu servers.

Example Playbook
----------------
```yml
- hosts: all
  become: true
  roles:
    - tyumentsev4.pgbouncer
```
